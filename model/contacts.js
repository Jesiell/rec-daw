module.exports = function(){
  var db = require('./../libs/connect_db')();
  var Schema = require('mongoose').Schema;

  var contact = Schema({
    nome: String,
    apelido: String,
    email: String,
    whatsapp: String
  });

  return contact;
}